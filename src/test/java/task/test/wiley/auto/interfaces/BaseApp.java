package task.test.wiley.auto.interfaces;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import utils.ConfigProperties;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.net.URL;

public class BaseApp {
    static WebDriver driver = null;
    static Logger rLogger = null;
    static Logger  tLogger = null;
    Actions builder;
    List<String> title = new ArrayList<String>();

    public BaseApp(Logger logger) {
        this.rLogger = logger;
    }
    public void setLogger(Logger tLogger){
        this.tLogger = tLogger;
    }

    public boolean init() throws Exception {
        //Start test on local machine
        rLogger.debug("Check environment");
        if (ConfigProperties.getTestProperty("environment").toLowerCase().equals("local")){
            rLogger.debug("Local environment, check browser");
            if (ConfigProperties.getTestProperty("browser").toLowerCase().equals("chrome")){
                rLogger.debug("Chrome browser. Start driver.");
                driver = new ChromeDriver();
                start();
            } else if (ConfigProperties.getTestProperty("browser").toLowerCase().equals("firefox")){
                rLogger.debug("Firefox browser. Start driver.");
                driver = new FirefoxDriver();
                start();
            } else {
                rLogger.warn("Unknown browser. Start chrome.");
                driver = new ChromeDriver();
                start();
            }
        }
        //Start test on remoute selenium server
        if (ConfigProperties.getTestProperty("environment").toLowerCase().equals("ci")){
            if (ConfigProperties.getTestProperty("browser").toLowerCase().equals("chrome")){
                rLogger.debug("Chrome browser. Start driver.");
                if (! startCI(BrowserType.CHROME)) return false;
            } else if (ConfigProperties.getTestProperty("browser").toLowerCase().equals("firefox")){
                rLogger.debug("Firefox browser. Start driver.");
                if (! startCI(BrowserType.FIREFOX)) return false;
            } else {
                rLogger.warn("Unknown browser. Start chrome.");
                if (! startCI(BrowserType.CHROME)) return false;
            }
        }
        if (driver == null){
            rLogger.error("Unknown " + ConfigProperties.getTestProperty("environment") + " environment");
            throw new Exception("Wrong environment");
        }
        builder = new Actions(this.driver);
        return true;
    }

    private boolean startCI(String bt) throws Exception {
        rLogger.debug("Set Browser");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setBrowserName(bt);
        rLogger.debug("Connect to remote selenium server");
        try {
            driver = new RemoteWebDriver(new URL(ConfigProperties.getTestProperty("seleniumServer")),capabilities);
        } catch (Exception ex){
            rLogger.error("Wrong selenium server "+ ex.getMessage());
            return false;
        }
        start();
        return true;
    }

    private void start() {
        rLogger.debug("Preparation browser");
        driver.get(ConfigProperties.getTestProperty("url"));
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
    }

    public void stop() {
        rLogger.debug("Close browser");
        driver.quit();
    }

    public BaseApp goToLink(String linkdText) {
        tLogger.debug("Go to " + linkdText + " link");
        driver.findElement(By.linkText(linkdText)).click();
        return this;
    }

    public BaseApp clickToLogo() {
        tLogger.debug("Click to logo");
        driver.findElement(By.cssSelector("a > .js-responsive-image")).click();
        return this;
    }

    public BaseApp search() {
        tLogger.debug("Click search button");
        builder.moveToElement(driver.findElement(By.cssSelector(".glyphicon")))
                .click()
                .build()
                .perform();
        return this;
    }

    public BaseApp cleanSearchText() {
        tLogger.debug("Clean search area");
        driver.findElement((By.id("js-site-search-input"))).clear();
        return this;
    }

    public BaseApp enterSearchText(String str) {
        tLogger.debug("Input " + str + " in search area and pause");
        builder.moveToElement(driver.findElement(By.id("js-site-search-input")))
                .click()
                .sendKeys(str)
                .pause(1500)
                .build()
                .perform();
        return this;
    }

    public String getMenuElementText(String linkText) {
        try {
            tLogger.debug("Find element " + linkText);
            return driver.findElement(By.linkText(linkText)).getText();
        } catch (org.openqa.selenium.NoSuchElementException ex) {
            // TO-DO Need Logger
            // Done
            tLogger.warn("Ekement not found "+ ex.getMessage());
            return "";
        }
    }

   public BaseApp goToElement(String linkText){
        tLogger.debug("Move cursor to element " + linkText + " and pause");
       builder.moveToElement(driver.findElement(By.linkText(linkText)));
       builder.pause(220).build().perform();
       return this;
   }

    public List<String> getMenuElements(String id) {
        List<WebElement> topics;
        tLogger.debug("Find all line on drop-down menu");
        topics = driver.findElement(By.id(id)).findElements(By.className("dropdown-item"));
        List<String> list = new ArrayList<String>();
        for(WebElement e : topics){
            tLogger.debug("Get line name");
            list.add(e.getText());
            tLogger.debug("Menu line name \"" + e.getText() + "\"");
        }
        return list;
    }

    public String getPageURL() {
        tLogger.debug("Get current URL");
        return driver.getCurrentUrl();
    }

    public String studentPageHead() {
        tLogger.debug("Get head on students page");
        return  driver.findElement(By.cssSelector(".sg-title-h1 > span:nth-child(1)")).getText();
    }
    public String getURLElement(String css){
        tLogger.debug("Get url on element");
        String link = driver.findElement(By.cssSelector(css))
                .getAttribute("href");
        tLogger.debug("Url on element " + link);
        return link;
    }

    public String educationPageHead() {
        tLogger.debug("Get head on education page");
        return driver.findElement(By.cssSelector("h1:nth-child(3) > span")).getText();
    }

    public List<String> getEducationSubjects() {
        tLogger.debug("Get subjects menu line on education page");
        List<WebElement> el = driver.findElement(By.cssSelector(".side-panel")).
                findElement(By.xpath("./ul")).findElements(By.tagName("li"));
        List<String> title = new ArrayList<String>();
        for(WebElement e : el){
            tLogger.debug("Get menu line");
            title.add(e.getText());
            tLogger.debug("Menu line name \"" + e.getText() + "\"");
        }
        return title;
    }

    public boolean checkAboutHome() {
        tLogger.debug("Check home page");
        try {
            driver.findElement(By.className("owl-item"));
        }catch (org.openqa.selenium.NoSuchElementException ex) {
            tLogger.debug("It's not home page");
            return false;
        }
        return true;
    }

    public List<String> getSuggestionsLine() {
        List<String> suggestionsLine = new ArrayList<String>();
        tLogger.debug("Find all suggestions");
        List<WebElement> el = driver.findElement(By.id("ui-id-2"))
                .findElements(By.xpath("./section[1]/div/div[*]"));
        for (WebElement e : el){
            tLogger.debug("Get suggestion name");
            suggestionsLine.add(e.getText());
            tLogger.debug("Suggestion name \"" + e.getText() + "\"");
        }
        return suggestionsLine;
    }

    public List<String> getProductsLine() {
        List<String> suggestionsLine = new ArrayList<String>();
        tLogger.debug("Find all product line");
        List<WebElement> el = driver.findElement(By.id("ui-id-2"))
                .findElements(By.xpath("./section[2]/div/div[*]"));
        for (WebElement e : el){
            tLogger.debug("Get product name");
            suggestionsLine.add(e.getText());
            tLogger.debug("Product name \"" + e.getText() + "\"");
        }
        return suggestionsLine;
    }

    public boolean checkAutocomplete() {
        tLogger.debug("Check about autocomplete is visible");
        return driver.findElement(By.id("ui-id-2")).isDisplayed();
    }

    public List<String> getSearchTitle() {
        tLogger.debug("Find products after search");
        List<WebElement> products = driver.findElement(By.className("products-list"))
                .findElements(By.xpath("./section[*]"));
        for(WebElement e : products){
            tLogger.debug("Get product name");
            title.add(e.findElement(By.xpath("./div[2]/h3")).getText());
            tLogger.debug("Product name \"" + e.getText() + "\"");
        }
        return title;
    }


    public boolean eachTitleCanAdd() {
        tLogger.debug("Check add button for each item");
        tLogger.debug("Find each item on page");
        List<WebElement> products = driver.findElement(By.className("products-list"))
                .findElements(By.xpath("./section[*]"));
        boolean canAdd = true;
        for (WebElement el : products){
            tLogger.debug("Get next item");
            tLogger.debug("Find oll platform");
            List<WebElement> productRow = el.findElements(By.className("product-table-row"));
            tLogger.debug("believe that we can not add and check the ability to add");
            canAdd = false;
            for (WebElement e : productRow){
                tLogger.debug("Get current platform and find add button");
                try {
                    e.findElement(By.className("small-button"));
                    tLogger.debug("Current platform contains product");
                    canAdd = true;
                } catch (org.openqa.selenium.NoSuchElementException ex){
                    tLogger.debug("Cant find button. Cant add product in current platform");
                }
                tLogger.debug("Check at least 1 platform for purchase");
                if(!canAdd) return false;
            }
        }
        return canAdd;
    }
}
