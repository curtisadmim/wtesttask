package task.test.wiley.auto.tests;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import task.test.wiley.auto.interfaces.BaseApp;

public class BaseTest {
    static final Logger rootLogger = LogManager.getRootLogger();
    BaseApp app = new BaseApp(rootLogger);

    @BeforeClass
    public void start() throws Exception {
        if (!app.init()) return;
    }

    @AfterClass
    public void stop(){
        app.stop();
    }
}
