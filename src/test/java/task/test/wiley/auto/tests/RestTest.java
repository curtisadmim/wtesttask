package task.test.wiley.auto.tests;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.testng.annotations.Test;
import io.restassured.response.Response;
import utils.ConfigProperties;

import static io.restassured.RestAssured.get;
import static org.testng.Assert.*;

public class RestTest {
    final Logger tLogger = LogManager.getLogger(SameScript.class);

    @Test
    public void delayTest(){
        tLogger.debug("Start delay test");
        tLogger.info("Request to " + ConfigProperties.getTestProperty("restService") + "/delay/0");
        Response response = get(ConfigProperties.getTestProperty("restService") +"/delay/0");
        JSONObject obj = new JSONObject(response.asString().replace("\n", ""));
        assertEquals(response.getContentType(),"application/json");
        assertEquals(response.statusCode(),200);
        assertEquals(obj.getString("url"),"https://httpbin.org/delay/0");
    }
    @Test
    public void pngTest(){
        tLogger.debug("Start png test");
        tLogger.info("Request to " + ConfigProperties.getTestProperty("restService") + "/image/png");
        Response response = get(ConfigProperties.getTestProperty("restService")+"/image/png");
        tLogger.debug("Check response");
        assertEquals(response.getContentType(),"image/png");
        assertEquals(response.statusCode(),200);
    }
}
