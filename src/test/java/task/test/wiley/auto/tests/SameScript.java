package task.test.wiley.auto.tests;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.*;


public class SameScript extends BaseTest {
    final Logger tLogger = LogManager.getLogger(SameScript.class);
    @Test
    public void sameScript(){
        tLogger.info("Start same script");
        app.setLogger(tLogger);
        //step 1
            //to-do assert element
        tLogger.info("Step 1");
        assertEquals("WHO WE SERVE",app.getMenuElementText("WHO WE SERVE"));
        assertEquals("SUBJECTS",app.getMenuElementText("SUBJECTS"));
        assertEquals("ABOUT",app.getMenuElementText("ABOUT"));
        //step 2
            // assert element on deader "Who We Serve"
        tLogger.info("Step 2");
        app.goToElement("WHO WE SERVE");
        List<String> list = app.getMenuElements("Level1NavNode1");
        assertEquals(list.size(),12);
        List<String> menuItem = new ArrayList<String>();
        menuItem.add("Students");
        menuItem.add("Instructors");
        menuItem.add("Book Authors");
        menuItem.add("Professionals");
        menuItem.add("Researchers");
        menuItem.add("Institutions");
        menuItem.add("Librarians");
        menuItem.add("Corporations");
        menuItem.add("Societies");
        menuItem.add("Journal Editors");
        menuItem.add("Bookstores");
        menuItem.add("Government");
        for(int i =0; i<12; i++ ){
            assertEquals(list.get(i), menuItem.get(i));
        }
        //step 3
        tLogger.info("Step 3");
        app.goToElement("Students").goToLink("Students");
        //TO-DO check students page
        assertEquals(app.getPageURL(),"https://www.wiley.com/en-ru/students");
        assertEquals(app.studentPageHead(),"Students");
        //TO-DO check "Learn More" link
        assertTrue(app.getURLElement("p:nth-child(9) > a").contains("www.wileyplus.com"));
        assertTrue(app.getURLElement("p:nth-child(16) > a").contains("www.wileyplus.com"));
        assertTrue(app.getURLElement("p:nth-child(21) > a").contains("www.wileyplus.com"));
        //step 4
        tLogger.info("Step 4");
        //check Subject Education element
        app.goToElement("SUBJECTS").goToElement("Education").goToLink("Education");
        assertEquals(app.educationPageHead(),"Education");
        menuItem.clear();
        menuItem.add("Information & Library Science");
        menuItem.add("Education & Public Policy");
        menuItem.add("K-12 General");
        menuItem.add("Higher Education General");
        menuItem.add("Vocational Technology");
        menuItem.add("Conflict Resolution & Mediation (School settings)");
        menuItem.add("Curriculum Tools- General");
        menuItem.add("Special Educational Needs");
        menuItem.add("Theory of Education");
        menuItem.add("Education Special Topics");
        menuItem.add("Educational Research & Statistics");
        menuItem.add("Literacy & Reading");
        menuItem.add("Classroom Management");
        list = app.getEducationSubjects();
        for(int i=0; i<menuItem.size(); i++){
            assertEquals(list.get(i), menuItem.get(i));
        }
        //step 5
        tLogger.info("Step 5");
        app.clickToLogo();
        //Check HomePage
        assertTrue(app.checkAboutHome());
        //step 6
        tLogger.info("Step 6");
        app.search();
            //TO-DO check current page
        assertTrue(app.checkAboutHome());
        //step 7
        tLogger.info("Step 7");
        //TO-DO check Area with related content is displayed right under the search header
        app.enterSearchText("java");
        assertTrue(app.checkAutocomplete());
        List<String> autocompliteItem;
        autocompliteItem = app.getSuggestionsLine();
        assertEquals(autocompliteItem.size(),4);
        for(int i =0; i<4; i++){
            assertTrue(autocompliteItem.get(i).substring(0,4).equals("java"));
        }
        autocompliteItem.clear();
        autocompliteItem = app.getProductsLine();
        assertEquals(autocompliteItem.size(),4);
        for(int i =0; i<4; i++){
            assertTrue(autocompliteItem.get(i).toLowerCase().contains("java"));
        }
        //step 8
        tLogger.info("Step 8");
        app.search();
        List<String> searchResult = app.getSearchTitle();
        assertEquals(searchResult.size(),10);
        for (String line : searchResult){
            assertTrue(line.toLowerCase().contains("java"));
        }
        app.eachTitleCanAdd();
        assertTrue(app.eachTitleCanAdd());
            //TO-DO Check result
        //step 9
        tLogger.info("Step 9");
        app.cleanSearchText().enterSearchText("java").search();
        List<String> newSearchResult = app.getSearchTitle();
        assertEquals(searchResult,newSearchResult);
            //TO-DO Check result on step 8
        tLogger.info("Finish script");
    }
}
